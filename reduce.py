"""

"""
import os
import logging
import argparse
import datetime
from multiprocessing import Pool, Queue

import numpy as np
from skvideo.io import FFmpegReader

LOGGER = logging.getLogger(__name__)


def get_max_color(frame):
    """
    
    Args:
        frame: 

    Returns:

    """
    flat_frame = frame.reshape((np.prod(frame.shape[0:2]), frame.shape[2]))
    color_hist, _ = np.histogramdd(flat_frame, bins=(255, 255, 255))
    max_color = np.unravel_index(np.argmax(color_hist), color_hist.shape)
    return max_color


def get_avg_color(frame):
    """
    
    Args:
        frame: 

    Returns:

    """
    avg_r = int(np.average(frame[:, :, 0]))
    avg_g = int(np.average(frame[:, :, 1]))
    avg_b = int(np.average(frame[:, :, 2]))
    avg_color = (avg_r, avg_g, avg_b)
    return avg_color


def get_avg_horiz_color(frame):
    """
    
    Args:
        frame: 

    Returns:

    """
    avg_color = np.round(np.average(frame, axis=1)).astype('i2')
    return avg_color


def worker(func, q_in, q_out):
    while True:
        i, frame = q_in.get()
        color = func(frame)
        q_out.put((i, color))


def create_processing_queues(max_queue_size=20):
    q_in = Queue(max_queue_size)
    q_out = Queue()
    return q_in, q_out


def reduce_video(filename, method="max", max_workers=6):
    """
    
    Args:
        filename: 
        method: 
        num_proc: 

    Returns:

    """
    start_time = datetime.datetime.now()

    vid = FFmpegReader(filename)
    num_frames, vid_height, vid_width = vid.getShape()[0:3]

    _methods = {'avg': (get_avg_color, 1), 'max': (get_max_color, 1), 'avg-horiz': (get_avg_horiz_color, 2)}
    agg_func, out_dims = _methods[method]

    percentages = list(range(1, 101))

    q_in, q_out = create_processing_queues(max_queue_size=24)

    with Pool(max_workers, worker, (agg_func, q_in, q_out)) as _:
        for i, frame in enumerate(vid.nextFrame()):
            percent = i*100 // num_frames
            q_in.put((i, frame.astype('i2')))
            if percent > percentages[0]:
                if len(percentages) > 1:
                    print("{}%".format(percentages[0]), end="\r")
                else:
                    print("{}%".format(percentages[0]))
                percentages.pop(0)

        if out_dims == 1:
            shape = (1080, num_frames, 3)
        elif out_dims == 2:
            shape = (vid_height, num_frames, 3)
        color_array = np.empty(shape, dtype='i2')
        while not q_out.empty():
            i, color = q_out.get()
            if out_dims == 1:
                color_array[:, i, :] = color[np.newaxis, :]
            elif out_dims == 2:
                color_array[:, i, :] = color[:, :]

    stop_time = datetime.datetime.now()
    print('Total Runtime: {}'.format((stop_time - start_time)))

    return color_array


def parse_args():
    """
    
    Returns:

    """
    parser = argparse.ArgumentParser()
    parser.add_argument('filename', type=str)
    parser.add_argument('mode', choices=('avg', 'avg-horiz', 'max'))
    args = parser.parse_args()

    return args


def main():
    """

    Returns:

    """
    args = parse_args()
    filename = os.path.abspath(args.filename)
    if not os.path.exists(filename):
        LOGGER.error("{} does not exist!".format(filename))
    color_array = reduce_video(filename, args.mode)
    np.save(filename, color_array)


if __name__ == '__main__':
    main()
